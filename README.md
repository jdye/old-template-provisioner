Instructions
============
* Move to the vagrant directory and use vagrant to bring up the environment:

```bash
    cd vagrant
    vagrant up
```

That's it.  It should create the rpmbuild machine first, install the build toolchain, and start compiling and packaging everything in the rpm/rpmbuild/SRPMS/ directory.  Once that machine is finished it will bring up the app server, templator, and install the application web stack.  Once it is finished you can verify the app is running from the virtualbox host at http://localhost:7071/ (a vm port forward set up by vagrant to the same).

Other things you can do
-----------------------
* You can bring up and down the machines one at a time:
```bash
    vagrant destroy templator
    vagrant up templator
```

* You can re-run the provisioning steps, which is mostly puppet:
```bash
   vagrant provision rpmbuild
   vagrant provision templator
```

* You can build the machines without the provisioning steps:
```bash
   vagrant up --no-provision
```
