# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant::Config.run do |config|
  ### all vms will use the same fedora 16 base image
  config.vm.box_url = "fedora16-x64.box"

  ### boot with a GUI so you can see the screen. (Default is headless)
  # config.vm.boot_mode = :gui

  # Forward a port from the guest to the host, which allows for outside
  # computers to access the VM, whereas host only networking does not.

  # rpm build config
  config.vm.define :rpmbuild do |r| 
    r.vm.box = "rpmbuild-fedora16-x64"

    r.vm.provision :puppet do |puppet|
      puppet.manifests_path = "../puppet/manifests"
      puppet.manifest_file  = "rpmbuild.pp"
      puppet.module_path = "../puppet/modules"
    end

    # will build all the rpms inside the provision step
    r.vm.provision :shell, :path => "../rpm/rpmbuild/helpers/buildall-vagrant-launch.sh"

    r.vm.share_folder "rpm-data", "/rpm", "../rpm"
  end

  # templator config
  config.vm.define :templator do |t|
    t.vm.box = "templator-fedora16-x64"
    t.vm.forward_port 80, 8081
    t.vm.forward_port 7071, 7071

    t.vm.provision :puppet do |puppet|
      puppet.manifests_path = "../puppet/manifests"
      puppet.manifest_file  = "templator.pp"
      puppet.module_path = "../puppet/modules"
      # puppet.options = "--verbose --debug"
    end

    t.vm.share_folder "rpm-data", "/rpm", "../rpm"
    t.vm.share_folder "src-data", "/src", "../src"
  end

  ###################
  # foregone config
  ###################

  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.
  # config.vm.network :hostonly, "192.168.33.10"

  # Assign this VM to a bridged network, allowing you to connect directly to a
  # network using the host's network device. This makes the VM appear as another
  # physical device on your network.
  # config.vm.network :bridged

  # Share an additional folder to the guest VM. The first argument is
  # an identifier, the second is the path on the guest to mount the
  # folder, and the third is the path on the host to the actual folder.
  # config.vm.share_folder "v-data", "/vagrant_data", "../data"
end

# vim: set ts=2 sw=2 expandtab:
