#!/bin/bash

if [ `id -u` == "0" ]; then
	su vagrant -c /rpm/rpmbuild/helpers/buildall.sh
else 
	/rpm/rpmbuild/helpers/buildall.sh
fi

exit 0;
