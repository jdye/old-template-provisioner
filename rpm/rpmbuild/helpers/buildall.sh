#!/bin/bash -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

echo "installing srpms"
for srcrpm in $DIR/../SRPMS/*.rpm; do 
	basename $srcrpm
	rpm -i $srcrpm
done

cd $DIR/../SOURCES/

echo "fetching install sources"
for spec in ../SPECS/*.spec; do
	spectool -gf $spec
done

popd

pushd $DIR/../SPECS/
for b in `cat $DIR/buildorder`; do
	echo building $b
	sudo yum-builddep -y $b
	rpmbuild -ba $b; 
	if [ $? -ne 0 ]; then
		echo build failed for $b - check build order 1>/dev/null 1>&2 
	fi 
	sudo yum install -y ../RPMS/*/*.rpm
done

cd $DIR/../RPMS/
createrepo .
