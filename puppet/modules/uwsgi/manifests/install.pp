class uwsgi::install {
    case $::operatingsystem {
        fedora: {
            $package_list = [ 'uwsgi', 'uwsgi-plugin-python' ]
            $username = 'uwsgi'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    Package {
        ensure => present,
    }

    File {
        require => User[$username]
    }

    package { $package_list: }

    user { $username:
        home    => "/home/$username",
        shell   => "/sbin/nologin"
    }

    group { $username:
        require => User[$username]
    }

    file { '/etc/uwsgi':
        ensure => directory,
        recurse => true,
    }

    file { '/etc/uwsgi/vassals':
        ensure => directory,
        recurse => true,
    }

    file { '/run/uwsgi/':
        mode => 0770,
        ensure => directory,
        owner => 'uwsgi',
        group => 'nginx',
    }

    Class['nginx::install'] -> Class['uwsgi::install']
}

# vim: set ts=4 ts=4 expandtab:
