class uwsgi ( $env = 'production' ) { 
	include uwsgi::install, uwsgi::nginx, uwsgi::service

    class { 'uwsgi::config':
        env => $env
    }
}

# vim: set ts=4 ts=4 expandtab:
