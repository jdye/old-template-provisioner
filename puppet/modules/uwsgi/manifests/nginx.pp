class uwsgi::nginx {
    case $::operatingsystem {
        fedora: {
            $nginx_conf = '/etc/nginx/conf.d/uwsgi.conf'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    File { 
        ensure => present,
        owner => 'uwsgi', 
        group => 'nginx', 
        mode => '0644',
    }

    file { '/nginx/wsgi/':
        ensure => directory,
        mode => 0755,
    }

    Class['uwsgi::config'] -> Class['uwsgi::nginx']
}

# vim: set ts=4 ts=4 expandtab:
