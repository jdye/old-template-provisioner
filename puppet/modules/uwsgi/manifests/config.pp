class uwsgi::config ( $env = 'production' ) {
    if $env in [ 'production', 'development' ] {
        $noop = 1
    } else {
        fail('env parameter must be \"production\" or \"development\"')
    }

    case $::operatingsystem {
        fedora: {
            $uwsgi_conf = '/etc/uwsgi/emperor.ini'
            $username = 'uwsgi'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    File { 
        owner => $username,
        group => $username,
        ensure => 'present',
        notify => Class['uwsgi::service'],
    }
    
    file { $uwsgi_conf: 
        content => template('uwsgi/emperor.ini.erb')
    }

    Class['uwsgi::install'] -> Class['uwsgi::config']
}

# vim: set ts=4 ts=4 expandtab:
