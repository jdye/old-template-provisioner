class uwsgi::service {
    case $::operatingsystem {
        fedora: {
            $service_name = [ 'emperor.uwsgi.service' ]
            $provider = 'systemd'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    Service {
        ensure => running, 
        provider => $provider, 
        hasstatus => true, 
        hasrestart => true, 
        enable => true 
    }

    service { $service_name: }
    
    Class['uwsgi::config'] -> Class['uwsgi::service']
}

# vim: set ts=4 ts=4 expandtab:
