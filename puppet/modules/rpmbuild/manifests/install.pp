class rpmbuild::install ($owner = 'vagrant') {
	case $::osfamily {
		RedHat: {
			$noop = 1
		}
		default: {
			fail("non-redhat operating system ${::operatingsystem} not supported by ${module_name}") 
		}
	}

	case $::operatingsystem {	
		Fedora: {
			$package_list = [ "rpm-build", "rpmdevtools", "yum-utils", "createrepo" ]
			$dev_group = "Development tools"
		}
		default: {
			fail("${module_name} does not support ${::operatingsystem}")
		}
	}

	Package {
		ensure => present,
	}
	
	package { $package_list: }

	exec { 'yum groupinstall':
		unless => "/usr/bin/yum grouplist \"$dev_group\" | /bin/grep \"^Installed Groups\"",
		command => "/usr/bin/yum -y groupinstall \"$dev_group\"",
        timeout => 0,
	}

    File {
        owner => $owner,
        group => $owner,
    }

    file { "/home/$owner/specs":
        ensure => link,
        target => '/rpm/rpmbuild/SPECS/',
    }

    file { "/home/$owner/.rpmmacros":
        ensure => present,
        source => [
            "puppet:///modules/rpmbuild/rpmmacros.$::operatingsystem.$::operatingsystemrelease",
            "puppet:///modules/rpmbuild/rpmmacros.$::operatingsystem",
            "puppet:///modules/rpmbuild/rpmmacros",
        ]
    }

    file { "/root/.rpmmacros":
        owner => 'root',
        group => 'root',
        ensure => present,
        source => [
            "puppet:///modules/rpmbuild/rpmmacros.$::operatingsystem.$::operatingsystemrelease",
            "puppet:///modules/rpmbuild/rpmmacros.$::operatingsystem",
            "puppet:///modules/rpmbuild/rpmmacros",
        ]
    }
}

# vim: set ts=4 ts=4 expandtab:
