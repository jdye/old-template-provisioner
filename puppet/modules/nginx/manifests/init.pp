class nginx {
    include nginx::install, nginx::config, nginx::service
}

# vim: set ts=4 ts=4 expandtab:
