class nginx::service {
    case $::operatingsystem {
        fedora: {
            $service_name = 'nginx.service'
            $provider = 'systemd'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    Service { 
        ensure => running, 
        provider => $provider, 
        hasstatus => true, 
        hasrestart => true, 
        enable => true 
    }

    service { $service_name: }

    Class['nginx::config'] -> Class['nginx::service']
}

# vim: set ts=4 ts=4 expandtab:
