class nginx::install {
    case $::operatingsystem {
        fedora: {   
            $package_list = 'nginx'
            $username = 'nginx'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    Package { ensure => present, }

    File {
        mode => 0755,
        owner => $username,
        group => $username,
    }

    package { $package_list: }

    file { '/nginx/':
        ensure => directory,
        recurse => true,
    }
}

# vim: set ts=4 ts=4 expandtab:
