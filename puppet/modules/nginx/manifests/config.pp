class nginx::config ($logstash = True, $env = 'production') {
    # validation
    if $logstash in [ True, False ] {
        $noop = 1
    } else {
        fail('logstash parameter must be True or False')
    }

    if $env in [ 'production', 'development' ] {
        $noop2 = 1
    } else {
        fail('env parameter must be \"production\" or \"development\"')
    }

    # operating system parameters
    case $::operatingsystem {
        fedora: {
            $config_file = '/etc/nginx/nginx.conf'
            $log_conf = '/etc/nginx/conf.d/logstash-logging.conf'
            $remove_files = [ '/etc/nginx/conf.d/default.conf', '/etc/nginx/conf.d/ssl.conf', '/etc/nginx/conf.d/virtual.conf' ]
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    # resource overrides
    File { 
        owner => 'nginx',    
        group => 'nginx', 
        ensure => present, 
        notify => Class['nginx::service'] 
    }

    # resources
    file { $config_file:
        mode => '0644',
        content => template('nginx/nginx.conf.erb')
    }

    if ($logstash == True) {
        file { $log_conf: 
            mode => '0644',
            content => template('nginx/logstash-logging.conf.erb')
        }
    }

    file { $remove_files:
        ensure => absent,
    }

    # dependencies
    Class['nginx::install'] -> Class['nginx::config']
}

# vim: set ts=4 ts=4 expandtab:
