class python::install {
    case $::operatingsystem {
        fedora: {   
            $package_list = [ 'python', 'python-pip', 'python-virtualenv' ]
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    Package { ensure => present, }
    
    package { $package_list: }
}

# vim: set ts=4 ts=4 expandtab:
