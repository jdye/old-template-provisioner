class vagrantenv::repo {
    Yumrepo {
        notify => Exec['yum-clean'],
        enabled => 1,   
        gpgcheck => 0,
        priority => 1,
        require => Package['yum-plugin-priorities'],
    }

    Package {
        ensure => present,
    }

    package { 'yum-plugin-priorities': }

    yumrepo { 'vagrant-local':
        descr => 'Vagrant local repo', 
        baseurl => 'file:///rpm/rpmbuild/RPMS/',
    }

    exec { 'yum-clean':
        user => 'root',
        path => '/usr/bin',
        command => 'yum clean metadata',
        refreshonly => true,
    }
}

# vim: set ts=4 ts=4 expandtab:
