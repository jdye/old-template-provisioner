class templator::install ( $username = 'nginx' ) {
    case $::operatingsystem {
        fedora: {   
            $package_list = 'templator'
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }

    Package {
        ensure => present
    }   

    File {
        owner => $username,
        group => $username,
    }

    package { $package_list: }

    file { '/nginx/wsgi/templator/':
        ensure => directory,
        mode => 0755,
    }
}

# vim: set ts=4 ts=4 expandtab:
