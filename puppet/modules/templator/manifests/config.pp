class templator::config ( $username = 'nginx', $env = 'production' ) {
    if $env in [ 'production', 'development' ] {
        $noop = 1
    } else {
        fail('env parameter must be \"production\" or \"development\"')
    }

    case $::operatingsystem {
        fedora: {   
        }
        default: {
            fail("Module ${module_name} does not support ${::operatingsystem}")
        }
    }
    
    File {
        ensure => present,
        mode => '0644',
        owner => $username,
        group => $username,
    }

    file { "/etc/nginx/conf.d/templator.conf":
        content => template("templator/nginx-templator.conf.erb"),
        require => Class['nginx::install'],
        notify => Class['nginx::service']
    }

    file { "/nginx/wsgi/templator/$env.ini": 
        content => template("templator/$env.ini.erb")
    }

    Class['templator::install'] -> Class['templator::config']
}

# vim: set ts=4 ts=4 expandtab:
