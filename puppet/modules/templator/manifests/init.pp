class templator ( $username = 'nginx' ) {
	class { 'templator::install': 
		username => $username
	}
	class { 'templator::config': 
		username => $username
	}
	class { 'templator::db': 
		username => $username
	}
}

# vim: set ts=4 ts=4 expandtab:
