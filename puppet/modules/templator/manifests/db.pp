class templator::db ( $username = 'nginx' ) {
	exec { "initialize_templator_db":
        command => "/usr/bin/initialize_templator_db production.ini",
        cwd => "/nginx/wsgi/templator/",
        creates => "/nginx/wsgi/templator/templator.sqlite",
        user => $username,
        
    }

    Class['templator::config'] -> Class['templator::db']
}

# vim: set ts=4 ts=4 expandtab:
