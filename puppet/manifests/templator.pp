# define convenience class for vagrant nodes
class templator_node {
    include nginx
    include python
    include pyramid
    include vagrantenv
    include uwsgi
    include templator

    Class['vagrantenv::repo'] -> Class['uwsgi']
}

# apply the class to all nodes
class {'templator_node': }

# vim: set ts=4 ts=4 expandtab:
