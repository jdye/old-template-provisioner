# define convenience class for vagrant nodes
class rpmbuild_node {
    include rpmbuild
}

# apply the class to all nodes
class {'rpmbuild_node': }

# vim: set ts=4 ts=4 expandtab:
